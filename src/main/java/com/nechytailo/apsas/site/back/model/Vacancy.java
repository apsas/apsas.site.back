package com.nechytailo.apsas.site.back.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class Vacancy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String jobTitle;
    @Lob
    private String description;
    private double salary;
    private Date applicationDeadline;
}
