package com.nechytailo.apsas.site.back.controller;

import com.nechytailo.apsas.site.back.dto.VacancyDTO;
import com.nechytailo.apsas.site.back.mapper.VacancyMapper;
import com.nechytailo.apsas.site.back.service.VacancyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/vacancies")
@RequiredArgsConstructor
public class VacancyController {

    private final VacancyService vacancyService;
    private final VacancyMapper vacancyMapper;

    @GetMapping
    public ResponseEntity<List<VacancyDTO>> getAllVacancies() {
        List<VacancyDTO> vacancies = vacancyService.getAllVacancies();
        return new ResponseEntity<>(vacancies, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VacancyDTO> getVacancyById(@PathVariable Long id) {
        VacancyDTO vacancyDTO = vacancyService.getVacancyById(id);
        return new ResponseEntity<>(vacancyDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<VacancyDTO> createVacancy(@RequestBody VacancyDTO vacancyDTO) {
        VacancyDTO createdVacancy = vacancyService.createVacancy(vacancyDTO);
        return new ResponseEntity<>(createdVacancy, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateVacancy(@PathVariable Long id, @RequestBody VacancyDTO vacancyDTO) {
        vacancyService.updateVacancy(id, vacancyDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteVacancy(@PathVariable Long id) {
        vacancyService.deleteVacancy(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
