package com.nechytailo.apsas.site.back.service;

import com.nechytailo.apsas.site.back.dto.VacancyDTO;
import com.nechytailo.apsas.site.back.exception.EntityNotFoundException;
import com.nechytailo.apsas.site.back.mapper.VacancyMapper;
import com.nechytailo.apsas.site.back.model.Vacancy;
import com.nechytailo.apsas.site.back.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class VacancyService {

    private final VacancyRepository vacancyRepository;
    private final VacancyMapper vacancyMapper;

    @Autowired
    public VacancyService(VacancyRepository vacancyRepository, VacancyMapper vacancyMapper) {
        this.vacancyRepository = vacancyRepository;
        this.vacancyMapper = vacancyMapper;
    }

    public List<VacancyDTO> getAllVacancies() {
        return vacancyMapper.toDTOList(vacancyRepository.findAll());
    }

    public VacancyDTO getVacancyById(Long id) {
        Vacancy vacancy = vacancyRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Vacancy not found with id: " + id));

        return vacancyMapper.toDTO(vacancy);
    }

    public VacancyDTO createVacancy(VacancyDTO vacancyDTO) {
        Vacancy vacancy = vacancyMapper.toEntity(vacancyDTO);
        Vacancy savedVacancy = vacancyRepository.save(vacancy);
        return vacancyMapper.toDTO(savedVacancy);
    }

    public void updateVacancy(Long id, VacancyDTO vacancyDTO) {
        Vacancy existingVacancy = vacancyRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Vacancy not found with id: " + id));

        Vacancy updatedVacancy = vacancyMapper.toEntity(vacancyDTO);
        updatedVacancy.setId(id);

        vacancyRepository.save(updatedVacancy);
    }

    public void deleteVacancy(Long id) {
        vacancyRepository.deleteById(id);
    }
}