package com.nechytailo.apsas.site.back.dto;

import lombok.Data;

@Data
public class ResumeDTO {
    private String content;
}