package com.nechytailo.apsas.site.back.Utils;

public class Constants {
    public static final String AUTHORIZATION = "Authorization";
    public static final String USERNAME = "username";
    public static final String ROLE = "role";
    public static final String USER_ID = "user_id";
    public static final String CONFIRMATION = "confirmation";
    public static final String BEARER = "Bearer ";
}
