package com.nechytailo.apsas.site.back.mapper;

import com.nechytailo.apsas.site.back.dto.SkillDTO;
import com.nechytailo.apsas.site.back.model.Skill;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface SkillMapper {

    SkillMapper INSTANCE = Mappers.getMapper(SkillMapper.class);

    SkillDTO toDTO(Skill skill);

    Skill toEntity(SkillDTO skillDTO);
}
