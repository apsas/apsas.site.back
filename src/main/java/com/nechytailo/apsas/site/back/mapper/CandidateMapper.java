package com.nechytailo.apsas.site.back.mapper;

import com.nechytailo.apsas.site.back.dto.CandidateDTO;
import com.nechytailo.apsas.site.back.model.Candidate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CandidateMapper {

    CandidateMapper INSTANCE = Mappers.getMapper(CandidateMapper.class);

    @Mapping(source = "user.id", target = "userId")
    CandidateDTO toDTO(Candidate candidate);
    List<CandidateDTO> toDTOList(List<Candidate> vacancies);

    @Mapping(source = "userId", target = "user.id")
    Candidate toEntity(CandidateDTO candidateDTO);
}
