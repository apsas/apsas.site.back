package com.nechytailo.apsas.site.back.service;

import com.nechytailo.apsas.site.back.dto.CandidateDTO;
import com.nechytailo.apsas.site.back.exception.EntityNotFoundException;
import com.nechytailo.apsas.site.back.mapper.CandidateMapper;
import com.nechytailo.apsas.site.back.model.Candidate;
import com.nechytailo.apsas.site.back.model.Vacancy;
import com.nechytailo.apsas.site.back.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CandidateService {

    private final CandidateRepository candidateRepository;
    private final CandidateMapper candidateMapper;

    @Autowired
    public CandidateService(CandidateRepository candidateRepository, CandidateMapper candidateMapper) {
        this.candidateRepository = candidateRepository;
        this.candidateMapper = candidateMapper;
    }

    public List<CandidateDTO> getAllCandidates() {
        List<Candidate> candidates = candidateRepository.findAll();
        return candidateMapper.toDTOList(candidates);
    }

    public CandidateDTO getCandidateById(Long id) {
        Candidate candidate = candidateRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Candidate not found with id: " + id));
        return candidateMapper.toDTO(candidate);
    }

    public CandidateDTO createCandidate(CandidateDTO candidateDTO) {
        Candidate candidate = candidateMapper.toEntity(candidateDTO);
        Candidate savedCandidate = candidateRepository.save(candidate);
        return candidateMapper.toDTO(savedCandidate);
    }

    public CandidateDTO updateCandidate(Long id, CandidateDTO candidateDTO) {
        Candidate existingCandidate = candidateRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Candidate not found with id: " + id));

        Candidate updatedCandidate = candidateMapper.toEntity(candidateDTO);
        updatedCandidate.setId(id);
        Candidate savedCandidate = candidateRepository.save(updatedCandidate);

        return candidateMapper.toDTO(savedCandidate);
    }

    public void deleteCandidate(Long id) {
        candidateRepository.deleteById(id);
    }
}
