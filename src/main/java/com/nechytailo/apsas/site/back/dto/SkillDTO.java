package com.nechytailo.apsas.site.back.dto;

import lombok.Data;

@Data
public class SkillDTO {
    private String name;
    private String description;
}
