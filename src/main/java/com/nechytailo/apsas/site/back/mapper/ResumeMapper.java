package com.nechytailo.apsas.site.back.mapper;

import com.nechytailo.apsas.site.back.dto.ResumeDTO;
import com.nechytailo.apsas.site.back.model.Resume;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ResumeMapper {

    ResumeMapper INSTANCE = Mappers.getMapper(ResumeMapper.class);

    ResumeDTO toDTO(Resume resume);

    Resume toEntity(ResumeDTO resumeDTO);
}
