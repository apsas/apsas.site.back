package com.nechytailo.apsas.site.back.mapper;

import com.nechytailo.apsas.site.back.dto.VacancyDTO;
import com.nechytailo.apsas.site.back.model.Vacancy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring")
public interface VacancyMapper {

    VacancyMapper INSTANCE = Mappers.getMapper(VacancyMapper.class);

    VacancyDTO toDTO(Vacancy vacancy);

    List<VacancyDTO> toDTOList(List<Vacancy> vacancies);

    Vacancy toEntity(VacancyDTO vacancyDTO);
}