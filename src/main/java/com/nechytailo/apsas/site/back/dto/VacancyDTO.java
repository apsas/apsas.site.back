package com.nechytailo.apsas.site.back.dto;

import lombok.Data;

import java.util.Date;

@Data
public class VacancyDTO {
    private String jobTitle;
    private String description;
    private double salary;
    private Date applicationDeadline;
}