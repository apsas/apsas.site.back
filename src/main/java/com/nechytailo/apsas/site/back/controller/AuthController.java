package com.nechytailo.apsas.site.back.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {


    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/public/home")
    public String publicHome() {
        return "public/home";
    }

    @GetMapping("/secure/home")
    public String secureHome() {
        return "secure/home";
    }
}