package com.nechytailo.apsas.site.back.dto;

import lombok.Data;

@Data
public class CandidateDTO {
    private String userId;
    private String firstName;
    private String lastName;
    private String contactInfo;
}
