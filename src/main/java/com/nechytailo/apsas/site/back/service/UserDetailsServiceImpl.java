package com.nechytailo.apsas.site.back.service;

import com.nechytailo.apsas.site.back.model.UserCustom;
import com.nechytailo.apsas.site.back.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserCustom loadUserByUsername(String username) throws UsernameNotFoundException {
        return new UserCustom(userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username)));
    }
}
